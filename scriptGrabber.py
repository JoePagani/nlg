from lxml import html
from lxml.html import parse
import requests
import os
import string

def main(): 

	if not os.path.isdir('Scripts'):
   		os.makedirs('Scripts')

	page = requests.get('https://bigbangtrans.wordpress.com/series-1-episode-1-pilot-episode/')
	tree = html.fromstring(page.content)
	otherPages = tree.xpath('//div[@id="sidebar"]//ul//a/@href')

	for i in range(1, len(otherPages)):
		splitName = str(otherPages[i]).split('/')

		currPage = requests.get(otherPages[i])
		currTree = html.fromstring(currPage.content)
		with open('Scripts/' + splitName[3], 'w') as f:
			for line in currTree.xpath('//div[@class="entrytext"]//p'):
				f.write("%s\n" % line.xpath('normalize-space(.)'))

		print(splitName[3], " has been created")

if __name__ == '__main__':
  main()