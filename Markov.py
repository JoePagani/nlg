from random import random, choice

class State:
    
    def __init__(self, vals):
        self._neighbors, self.weights = zip(*vals.items())
        if sum(self.weights) > 2:
            print(f"ERROR: invalid probabilities: {self.weights}")

    def select_neighbor(self):
        r = random()
        min_val = 1
        n_index = 0
        prev = self.weights[0]

        for i in range(len(self.weights)):
            p = self.weights[i]
            if p == 0:
                continue
            elif p == 1:
                n_index = i
                break

            x = min(abs(p - r), min_val)
            
            if min_val > x:
                min_val = x
                n_index = i
            elif min_val == x and prev == p:
                n_index = choice([n_index, i])
            prev = p

        return self._neighbors[n_index]

    @property
    def json(self):
        return { k:v for k,v in zip(self._neighbors, self.weights) }
    

class MarkovChain:
    def __init__(self, continuous=False, seed=None):
        self._chain = {}
        self.current_state = State('initial_state', seed) if seed else None
        self.continuous = continuous
        #self.transition_matrix = []

    def add_state(self, key, probs):
        if key in self._chain:
            raise Exception('State already exists')

        self._chain[key] = State(probs)

    def start_state(self):
        #TODO: use seed
        self.current_state = choice(list(self._chain.keys()))
        return self.current_state

    def next_state(self):
        if self.current_state and self.current_state in self._chain:
            self.current_state = self._chain[self.current_state].select_neighbor()
            return self.current_state
        return self.start_state()

    def continuous_state(self):
        if self.current_state and self.current_state in self._chain:
            # Assuming TEXT AKA (isinstance(self.current_state, str) == True) and (isinstance(nxt_val, str) == True)
            nxt_val = self._chain[self.current_state].select_neighbor()
            sub_s = self.current_state + nxt_val
            self.current_state = sub_s[len(nxt_val):]
            return self.current_state[-len(nxt_val)]
        return self.start_state()


    def generate(self, num_of_stops, start=None):
        result = []
        if start:
            result.append(start)
            self.current_state = start
        
        if self.continuous:
            result.extend([self.continuous_state() for i in range(num_of_stops)])
        else:
            result.extend([self.next_state() for i in range(num_of_stops)])
        return result

    @property
    def json(self):
        return {k: v.json for k,v in self._chain.items()}
    