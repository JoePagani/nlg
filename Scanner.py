import collections
import re

Token = collections.namedtuple('Token', ['typ', 'value', 'attribute', 'line'])

class Scanner:
	"""Sources: https://www.ef.edu/english-resources/english-grammar"""
	def __init__(self):
		# Known characters
		self.keywords = {
			'Sheldon',
			'Howard',
    		'Penny',
    		'Leonard',
    		'Raj',
    		'Amy',
    		'Bernadette'
		}
		self.determiners = {
			'a',
			'an',
			'the',
			'this',
			'that',
			'these',
			'those',
			'few',
			'little',
			'much',
			'many',
			'most',
			'some',
			'any',
			'enough',
			'all',
			'both',
			'half',
			'either',
			'neither',
			'each',
			'every',
			'other',
			'another',

		}
		#self.verbs = {}?
		self.pronouns = {
			'i',			#Subject
			'me',			#Object		
			'my',			#Possessive Adjective		
			'mine',			#Possessive Pronoun		
			'myself',		#Reflexive		
			'you',			#Subject/Object
			'your',			#Possessive Adjective
			'yours',		#Possessive Pronoun
			'yourself',		#Reflexive
			'yourselves',	#Reflexive
			'he',			#Subject
			'him',			#Object		
			'his',			#Possessive Adjective
			'himself',		#Reflexive
			'she',			#Subject
			'her',			#Object/Possessive Adjective
			'hers',			#Possessive Pronoun
			'herself',		#Reflexive
			'it',			#Subject/Object
			'its',			#Possessive Adjective
			'itself',		#Reflexive
			'we',			#Subject
			'us',			#Object
			'our',			#Possessive Adjective
			'ours',			#Possessive Pronoun
			'ourselves',	#Reflexive
			'they',			#Subject
			'them',			#Object
			'their',		#Possessive Adjective
			'theirs',		#Possessive Pronoun
			'themselves',	#Reflexive
			'everyone',		#Indefinite/Person
			'everybody',	#Indefinite/Person
			'everywhere',	#Indefinite/Place
			'everything',	#Indefinite/Thing
			'someone',		#Indefinite/Person
			'somebody',		#Indefinite/Person
			'somewhere',	#Indefinite/Place
			'something',	#Indefinite/Thing
			'anyone',		#Indefinite/Person
			'anybody',		#Indefinite/Person
			'anywhere',		#Indefinite/Place
			'anything',		#Indefinite/Thing
			'nobody',		#Indefinite/Person
			'nowhere',		#Indefinite/Place
			'nothing'		#Indefinite/Thing
		}
		self.patterns = [
            ('NUMBER', r'\d+(\.\d+)?'),
            ('PUNCT', r'[!\.\?]'),
            ('DASH', r'\-'),
            ('COLON', r':'),
            ('QUOTE', r'[\'"]'),
            ('PAREN', r'[()]'),
            ('COMMA', r','),
            ('WORD', r'[A-Za-z]+'),
            ('NEWLINE', r'\r\n?|\n'),
            ('WHTSPC', r'[ \t]+'),
            ('MISMATCH', r'.')
        ]
		self.regex = '|'.join(f"(?P<{name}>{regex})" for name, regex in self.patterns)


	def tokenize(self, data):
		line_num = 1
		for m in re.finditer(self.regex, data):
			kind = m.lastgroup
			value = m.group(kind)
			attrib = None
			if kind == 'NEWLINE':
				line_num += 1
			elif kind == 'WORD':
				if value == 'Scene':
					kind = value
					attrib = value
				elif value in self.keywords:
					kind = 'CHARACTER'
					attrib = 'NOUN'
				elif value in self.pronouns:
					attrib = 'PRONOUN'
				elif value in self.determiners:
					attrib = 'DETERMINER'
			yield Token(kind, value, attrib, line_num)
