class Scene:
	"""docstring for Scene"""

	def __init__(self, ep_title):
		self.name = ep_title
		self.scenes = []

	def add_scene(self, details):
		self.scenes.append(details)

	def display(self):
		for n in range(len(self.scenes)):
			print(f'Scene number {n}, Details:')
			for d in self.scenes[n]:
				print(*d[1])

