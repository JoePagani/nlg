from Parser import Parser
from Scanner import Scanner
from Markov import MarkovChain
from pprint import pprint
from Script import Script
import os, sys

character_chains = {
    'Sheldon': [],
    'Howard': [],
    'Penny': [],
    'Leonard': [],
    'Raj': [],
    'Amy': [],
    'Bernadette': []
}

def update_char_chain(fname, fpath):
    global character_chains
    with open(fpath) as script:
        t = iter(tokenize(script.read()))

    scene_info, chars_info = parse(t, fname)
    for c in chars_info:
        if c.name in character_chains:
            character_chains[c.name].extend(c.get_raw_dialog())

def word_ngrams(text):
    ngram = {}
    for i in range(len(text) - 1):
        word = text[i]
        follow = text[i+1] if (i+1) < len(text) else text[i- len(text)]
        if word in ngram:
            ngram[word].append(follow)
        else:
            ngram[word] = [follow]
    return ngram

def char_ngrams(text, window=3):
    # clean text?
    ngrams = {}
    #gram_count = {}
    for i in range(len(text) - (window - 1)):
        gram = text[i : i + window]
        # returning 0 at the end of the string sorta "wraps" the text
        n_char_index =  (i + window) if (i + window) < len(text) else 0
        if gram in ngrams:
            ngrams[gram].append(text[n_char_index])
            #gram_count[gram] += 1
        else:
            ngrams[gram] = [text[n_char_index]]
            #gram_count[gram] = 1

    return ngrams

def probabilities(chars):
    total = len(chars)
    p_count = {}
    for c in chars:
        if c in p_count:
            p_count[c] += 1
        else:
            p_count[c] = 1
    return {k: v / total for k,v in p_count.items()}

def get_character(key, continuous=False, gen_size=50, start=None):
    ngrams = None
    
    if continuous:
        ngrams = char_ngrams(' '.join(character_chains.get(key, [])))
    else:
        buff = ''
        ngrams = word_ngrams(character_chains.get(key, []))

    M = MarkovChain(continuous)
    for k, v in ngrams.items():
        M.add_state(k, probabilities(v))
    if continuous:
        return ''.join(M.generate(gen_size, start))
    return ' '.join(M.generate(gen_size, start))

def parse_script(data):
    parser = Parser()
    scan = Scanner()
    return [i for i in parser.parse(scan.tokenize(data))]

def script_selection(char_info):
    for k,v in char_info.items():
        if k and v:
            total = sum(v.values())
            probs = {x: y/total for x, y in v.items()}
            yield k, probs

if __name__ == '__main__':
    parser = Parser()
    scan = Scanner()
    script = Script()
    season = os.path.abspath('Scripts/season6')
    with os.scandir(season) as d:
        for f in d:
            if f.is_file():
                #update_char_chain(f.name, f.path)
                with open(f) as fn:
                    for i in parser.parse(scan.tokenize(fn.read())):
                        script.add_scene(**i)
    '''
    for c in character_chains.keys():
        x = get_character(c, False, 10)
        print(f"{c}: {x}\n")
    fname = sys.argv[1]
    script = Script()
    with open(fname) as f:
        parsed = parse_script(f.read())
    
    for i in parsed:
        script.add_scene(**i)
    '''
    pprint(script.characters)
    M = MarkovChain()
    for k,p in script_selection(script.characters):
        M.add_state(k, p)
    pprint(M.generate(10))