from Scene import Scene
from Character import Character

class Script:

    def __init__(self):
        self.characters = {}

    def _add_character(self, prev_char, char_name):
        if char_name not in self.characters:
            self.characters[char_name] = {}
        
        if prev_char:
            if prev_char in self.characters[char_name]:
                self.characters[char_name][prev_char] += 1
            else:
                self.characters[char_name][prev_char] = 1


    def add_scene(self, setting='', dialog=[]):
        prev = None
        for c, line, act in dialog:
            self._add_character(prev, c)
            prev = c

