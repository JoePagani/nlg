class Character:
	"""docstring for Character"""
	def __init__(self, name, dialog, actions):
		self.name = name
		self.actions = []
		self.dialog = []
		self.add_traits(dialog, actions)

	def add_traits(self, dialog, actions):
		if dialog:
			self.dialog.extend(dialog)
		if actions:
			self.actions.extend(actions)

	def get_raw_dialog(self):
		raw_dialog = []
		for stmt, dial in self.dialog:
			raw_dialog.extend([w.lower() for w in dial])
		return raw_dialog

	def display(self):
		print(f'Character: {self.name}')
		if self.dialog:
			print("--->Dialog:")
			for line in self.dialog:
				print(f'\t\t{line[-1]}')
		if self.actions:
			print("--->Actions:")
			for act in self.actions:
				print(f'\t\t{act}')