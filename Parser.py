from Scene import Scene
from Character import Character

class Parser:

    def __init__(self):
        self.current = None
        self.tokens = None

    def advance(self):
        try:
            self.current = next(self.tokens)
        except StopIteration:
            self.current = None

    def match(self, typ, value=None):
        # Skip.
        if value is None and typ is None:
            val = self.current.value
            self.advance()
            return val

        # Match type.
        if value is None and self.current.typ == typ:
            val = self.current.value
            self.advance()
            return val

        # Match literal.
        if self.current.typ == typ and self.current.value == value:
            self.advance()
            return value

        raise RuntimeError(f'Expected a {typ}:( {value} ), got a {self.current.typ}( {self.current.value} ) line: {self.current.line}')

    def _skip_space(self, newlines=False):
        if self.current == None:
            return False
        if self.current.typ == 'WHTSPC':
            self.match('WHTSPC')
            return True
        if newlines and self.current.typ == 'NEWLINE':
            self.match('NEWLINE')
            return True
        return False

    def _is_scene(self):
        self.match('Scene')
        self._skip_space()
        if self.current.typ == 'COLON':
            self.match('COLON')
            self._skip_space()
            return True
        return False

    def _period(self):
        val = self.match('PUNCT', '.')
        if self.current.typ == 'WORD':
            #Probably an Abbreviation
            return val
        if self.current.typ == 'WHTSPC' or self.current.typ == 'NEWLINE':
            #Probably end of sentence
            return val
        if self.current.typ == 'PUNCT' and self.current.value == '.':
            #Ellipsis
            return (val, self._period())
        return None

    def _scene_setting(self):
        setting = []
        while self.current.typ != 'NEWLINE':
            setting.append(self.match(None))
        self.match('NEWLINE')
        return ''.join(setting)

    def _action(self):
        self.match('PAREN', '(')
        action = []
        while self.current.typ != 'PAREN':
            action.append(self.match(None))
        self.match('PAREN', ')')
        return ''.join(action)

    def _character(self):
        character = []
        actions = []
        while True:
            if self.current.typ == 'COLON':
                self.match('COLON')
                self._skip_space()
                break
            elif self.current.typ == 'NEWLINE':
                #TODO: this crap -> series-10-episode-04 line 91
                actions.extend(character)
                character = []
                break
            elif self.current.typ == 'PAREN':
                actions.append(self._action())
            elif self.current.typ == 'CHARACTER':
                character.append(self.match('CHARACTER'))
            elif self.current.typ == 'WORD' and self.current.attribute is None:
                character.append(self.match('WORD'))
            else:
                self.match(None)
        return character, actions

    def _dialog(self):
        character, actions = self._character()
        line = []
        while True:
            if self.current == None:
                break
            elif self.current.typ == 'PAREN':
                actions.append(self._action())
            elif self.current.typ == 'NEWLINE':
                self.match('NEWLINE')
                break
            else:
                line.append(self.match(None))
        #TODO: if multiple characters maybe spread instead of creating a new key
        return ''.join(character), ''.join(line), actions

    def scene(self):
        end_scene = False
        setting = self._scene_setting()
        dialog = []
        while not end_scene:
            if self.current is None:
                break
            elif self.current.typ == 'Scene':
                end_scene = self._is_scene()
            else:
                dialog.append(self._dialog())
        return {
            'setting': setting,
            'dialog': dialog
        }

    def pre_text(self):
        pre = []
        while self.current.typ != 'Scene':
            pre.append(self.match(None))
        return ''.join(pre)

    def parse(self, tokens):
        self.tokens = tokens
        self.advance()
        prescript = None
        if self.current.typ != 'Scene':
            prescript = self.pre_text()
        while self.current != None:
            yield self.scene()

"""
def parse(tokens, script_name="bigbang"):

    current = next(tokens)

    def match(typ, value=None):
        if value == None and current.typ == typ:
            val = current.value
            advance()
            return val
        elif current.typ == typ and current.value == value:
            advance()
            return value

    def advance():
        nonlocal current
        try:
            current = next(tokens)
        except StopIteration:
            current = None

    def script():
        script_scenes = Scene(script_name)
        chars = {}
        while current != None:
            #print(f'{current.line}, script')
            if current.typ == 'SCENE':
                script_scenes.add_scene(scene())
            elif current.typ == 'CHARACTER':
                curr = character()
                if curr[0] not in chars.keys():
                    chars[curr[0]] = Character(curr[0], curr[1], curr[2])
                else:
                    chars[curr[0]].add_traits(curr[1], curr[2])
            elif current.typ == 'PAREN':
                curr = statements()
            else:
                advance()

        return (script_scenes, chars.values())

    def scene():
        #print(f'{current.line}, scene')
        match('SCENE', 'Scene')
        match('SEP', ':')
        scene_details = statements()
        return scene_details

    def character():
        
        char_name = match('CHARACTER')
        dialog = []
        actions = []

        while current.typ != 'SEP':
            #print(f'{current.line}, character')
            if current.typ == 'END':
                match('END', '$')
                break
            elif current.typ == 'WORD':
                char_name += " "
                char_name += match('WORD')
            elif current.typ == 'PAREN':
                act = action_stmt()
                actions.append(act[1])
            else:
                advance()
        
        match('SEP', ':')
        stmts = statements()
        
        for x in stmts:
            if x[0] == 'Action':
                actions.append(x[1])
            else:
                dialog.append(x)

        return (char_name, dialog, actions)

    def statements():
        stmts = []
        
        while current.typ != 'END':
            #print(f'{current.line}, statement')
            if current.typ == 'PAREN':
                stmts.append(action_stmt())
            else:
                stmts.append(sentence())
        match('END', '$')
        
        return stmts

    def sentence():
        sent = []
        
        while current.typ != 'STOP':
            #print(f'{current.line}, sentence ')
            if current.typ == 'WORD':
                sent.append(match('WORD'))
            elif current.typ == 'END':
                break
            else:
                advance()
        
        if current.typ != 'END':
            sent_end = end_punct()
            return (sent_end, sent)
        else:
            return ("No Punct", sent)

    def end_punct():
        end = match('STOP')
        
        if end == '!':
            return 'Exclamation'
        elif end == '?':
            return'Question'
        else:
            return 'Statement'

    def action_stmt():
        act_stmt = []
        match('PAREN', '(')
        
        while current.typ != 'PAREN':
            #print(f'{current.line}, action_stmt')
            if current.typ == 'WORD':
                act_stmt.append(match('WORD'))
            else:
                advance()
        
        match('PAREN', ')')
        
        return ('Action', act_stmt)

    return script()
"""